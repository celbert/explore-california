﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace ExploreCalifornia.Models;

public class Post
{

    public long Id { get; set; }
    
    [Required]
    [Display(Name = "Post Title")]
    [StringLength(100, MinimumLength = 5, ErrorMessage = "Title must be between 5 and 500 chars")]
    public string Title { get; set; }
    public string? Author { get; set; }
    
    [Required]
    [MinLength(100, ErrorMessage = "Blog posts must be at least 100 chars")]
    [DataType(DataType.MultilineText)]
    public string Body { get; set; }
    public DateTime Posted { get; set; }
    
}