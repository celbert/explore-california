﻿using ExploreCalifornia.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExploreCalifornia.Controllers;

[Route("blog")]
public class BlogController : Controller
{
    private readonly BlogDataContext _db;

    public BlogController(BlogDataContext db)
    {
        _db = db;
    }
    public IActionResult Index(int page = 0)
    {
        var pageSize = 2;
        var totalPosts = _db.Posts.Count();
        var totalPages = totalPosts / pageSize;
        var previousPage = page - 1;
        var nextPage = page + 1;

        ViewBag.PreviousPage = previousPage;
        ViewBag.HasPreviousPage = previousPage >= 0;
        ViewBag.NextPage = nextPage;
        ViewBag.HasNextPage = nextPage < totalPages;
        
        var posts = _db.Posts
            .OrderByDescending(x => x.Posted)
            .Skip(pageSize * page)
            .Take(pageSize)
            .ToArray();
        return View(posts);
    }

    [Route("{year:min(2000)}/{month:range(1,12)}/{id}")]
    public IActionResult Post(int year, int month, int id)
    {
        var post = _db.Posts.FirstOrDefault(x => x.Id == id);
        // ViewBag.Title = "My blog post";
        // ViewBag.Posted = DateTime.Now;
        // ViewBag.Author = "Chris Elbert";
        // ViewBag.Body = "This is a great blog post, don't you think?";
        return View(post);
    }

    [Route("create")]
    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }
    
    [Route("create")]
    [HttpPost]
    public IActionResult Create(Post post)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }
        
        post.Author = User.Identity.Name ?? "Default";
        post.Posted = DateTime.Now;

        _db.Posts.Add(post);
        _db.SaveChanges();

        return RedirectToAction("Post", "Blog", new
        {
            year = post.Posted.Year,
            month = post.Posted.Month,
            id = post.Id
        });
    }
}