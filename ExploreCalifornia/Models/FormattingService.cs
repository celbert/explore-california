namespace ExploreCalifornia.Models;

public class FormattingService
{
    public string AsFormattedDate(DateTime dateTime)
    {
        return dateTime.ToString("d");
    }
}