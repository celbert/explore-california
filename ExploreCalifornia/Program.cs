using ExploreCalifornia;
using ExploreCalifornia.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTransient<FeatureToggles>(x => new FeatureToggles()
{
    DeveloperExceptions = builder.Configuration.GetValue<bool>("FeatureToggles:DeveloperExceptions")
});
builder.Services.AddTransient<FormattingService>();

builder.Services.AddMvc(options => options.EnableEndpointRouting = false);

builder.Services.AddDbContext<BlogDataContext>(options =>
{
    options.UseInMemoryDatabase(databaseName: "explore-cali-db");
});


var app = builder.Build();

app.UseFileServer();
app.UseExceptionHandler("/error.html");


using (var serviceScope = app.Services.CreateScope())
{
    var services = serviceScope.ServiceProvider;
    var dep = services.GetRequiredService<FeatureToggles>();
    if (dep.DeveloperExceptions)
    {
        app.UseDeveloperExceptionPage();
    }

}

app.Use(async (context, next) =>
{
    if (context.Request.Path.Value.Contains("invalid"))
    {
        throw new Exception("ERROR!");
    }
    await next();
});


app.UseMvc(routes =>
{
    routes.MapRoute("Default", "{controller=Home}/{action=Index}/{id:int?}");
});

app.Run();

