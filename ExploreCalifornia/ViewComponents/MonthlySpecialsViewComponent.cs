using ExploreCalifornia.Models;
using Microsoft.AspNetCore.Mvc;

namespace ExploreCalifornia.ViewComponents;

public class MonthlySpecialsViewComponent : ViewComponent
{
    private readonly BlogDataContext _db;
    public MonthlySpecialsViewComponent(BlogDataContext db)
    {
        _db = db;
    }
    public IViewComponentResult Invoke()
    {
        var specials = _db.MonthlySpecials.ToArray();
        return View(specials);
    }
}